package com.sudomakmur.kalkulator

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sudomakmur.kalkulator.model.Pahlawan

class ListHeroAdapter(val listImages : ArrayList<Pahlawan>) : RecyclerView.Adapter<ListHeroAdapter.ListHeroViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHeroViewHolder {
       return ListHeroViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_hero,parent,false))
    }

    override fun getItemCount(): Int {
        return listImages.size
    }

    override fun onBindViewHolder(holder: ListHeroViewHolder, position: Int) {
       val listImage = listImages.get(position)

       listImage.let {
           Glide.with(holder.itemView).load(it.image).into(holder.imgHero)
           holder.textHero.text = it.description.substring(0,100)+"..."
       }

//        Log.i("TAGED", "onBindViewHolder: $listImage")
//        holder.imgHero.setImageResource(listImage)
    }

    inner class ListHeroViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val imgHero = view.findViewById<ImageView>(R.id.imgItemHero)
        val textHero = view.findViewById<TextView>(R.id.textItemHero)
    }
}