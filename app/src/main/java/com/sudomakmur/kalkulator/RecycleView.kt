package com.sudomakmur.kalkulator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sudomakmur.kalkulator.model.Pahlawan
import java.util.ArrayList

class RecycleView : AppCompatActivity() {

    var listHeroAdapter : ListHeroAdapter? = null
    val heroes = ArrayList<Pahlawan>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_view)
        val recycleView = findViewById<RecyclerView>(R.id.rcvImageHero)



        initiateData()
        val linearlayout = LinearLayoutManager(this)
        linearlayout.orientation = LinearLayoutManager.VERTICAL
        recycleView.layoutManager = linearlayout


//        recycleView.layoutManager = GridLayoutManager(this,3)

        listHeroAdapter = ListHeroAdapter(heroes)
        recycleView.adapter = listHeroAdapter




    }

    private fun initiateData() {
        val imageString = resources.getStringArray(R.array.data_photo_string)
        val descriptionString = resources.getStringArray(R.array.data_description)

        for (i in imageString.indices){
               heroes.add(Pahlawan(imageString.get(i),descriptionString.get(i)))
        }


    }
}