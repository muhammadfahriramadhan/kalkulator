package com.sudomakmur.kalkulator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView

class WebView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view2)


        val webView = findViewById<WebView>(R.id.webview2)


        webView.loadUrl("https://www.detik.com/")



    }
}